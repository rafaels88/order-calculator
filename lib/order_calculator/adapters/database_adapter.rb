module OrderCalculator
  class DatabaseAdapter
    attr_reader :adapter

    def initialize(sources:)
      @adapter = CsvDatabaseAdapter.new(sources: sources)
    end

    def read(data_source)
      adapter.read(data_source)
    end
  end
end
