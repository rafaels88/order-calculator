module OrderCalculator
  class CsvDatabaseAdapter
    attr_reader :sources, :datas

    def initialize(sources:)
      @sources = sources
      @file_handler = CsvFileHandler.new
      load_datas!
    end

    def read(data_source)
      datas[data_source]
    end

    private

    def load_datas!
      if @datas.nil?
        @datas = {}
        sources.each do |name, path|
          @datas[name] = normalize_data_results(@file_handler.read(path))
        end
      end
      @datas
    end

    def normalize_data_results(values)
      values
        .reject { |o| o.compact.empty? }
        .map { |v| { values: v } }
    end
  end
end
