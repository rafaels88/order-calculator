module OrderCalculator
  class CsvFileHandler
    def read(file_path)
      CSV.open(file_path, "r", :col_sep => ?,, :row_sep => ?\r).read

      rescue CSV::MalformedCSVError => e
        raise InvalidSourceFileException.new("An invalid CSV file (#{file_path}) were given. Details: #{e.message}")
    end

    def write(output_path, content)
      CSV.open(output_path, "wb", :col_sep => ?,, :row_sep => ?\r) do |csv|
        content.each do |line|
          csv << line
        end
      end

      rescue StandardError => e
        raise OutputResultException.new("Some problem with write the CSV file (#{output_path}). Details: #{e.message}")
    end
  end
end
