module OrderCalculator
  class ProductData
    DATA_SOURCE = :products.freeze

    attr_reader :data, :database_adapter

    def initialize(data:, database_adapter:)
      @database_adapter = database_adapter
      @data = data
    end

    def id
      data[:values][0]
    end

    def price
      data[:values][1].to_f if data[:values][1]
    end
  end
end
