module OrderCalculator
  class OrderItemData
    DATA_SOURCE = :order_items.freeze

    attr_reader :data, :database_adapter

    def initialize(data:, database_adapter:)
      @database_adapter = database_adapter
      @data = data
    end

    def order_id
      data[:values][0]
    end

    def product_id
      data[:values][1]
    end

    def product
      if @_product.nil?
        products = database_adapter.read(ProductData::DATA_SOURCE).map do |product_data|
          product = ProductData.new(data: product_data, database_adapter: database_adapter)
          product if product.id == product_id
        end
        @_product = products.compact.first
      end
      @_product
    end

    def order
      if @_order.nil?
        orders = database_adapter.read(OrderData::DATA_SOURCE).map do |order_data|
          order = OrderData.new(data: order_data, database_adapter: database_adapter)
          order if order.id == order_id
        end
        @_order = orders.compact.first
      end
      @_order
    end
  end
end
