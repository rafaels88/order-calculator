module OrderCalculator
  class CouponData
    DATA_SOURCE = :coupons.freeze

    attr_reader :data, :database_adapter

    def initialize(data:, database_adapter:)
      @database_adapter = database_adapter
      @data = data
    end

    def id
      data[:values][0]
    end

    def expiry_date
      Date.strptime(data[:values][3], "%Y/%m/%d")
    end

    def discount_type
      data[:values][2]
    end

    def value
      data[:values][1].to_f if data[:values][1]
    end

    def allowed_times
      data[:values][4].to_i if data[:values][4]
    end
  end
end
