module OrderCalculator
  class OrderData
    DATA_SOURCE = :orders.freeze

    attr_reader :data, :database_adapter

    def initialize(data:, database_adapter:)
      @database_adapter = database_adapter
      @data = data
    end

    def id
      data[:values][0]
    end

    def coupon_id
      data[:values][1]
    end

    def items
      if @_items.nil?
        results = database_adapter.read(OrderItemData::DATA_SOURCE).map do |item_data|
          order_item = OrderItemData.new(data: item_data, database_adapter: database_adapter)
          order_item if order_item.order_id == id
        end
        @_items = results.compact
      end
      @_items
    end

    def coupon
      if @_coupons.nil?
        coupons = database_adapter.read(CouponData::DATA_SOURCE).map do |coupon_data|
          coupon = CouponData.new(data: coupon_data, database_adapter: database_adapter)
          coupon if coupon.id == coupon_id
        end
        @_coupons = coupons.compact.first
      end
      @_coupons
    end
  end
end
