module OrderCalculator
  class Repository
    attr_reader :database_adapter

    def initialize(database_adapter:)
      @database_adapter = database_adapter
    end

    def all_orders
      all_order_datas.map do |order_data|
        order_data_instance = OrderData.new(data: order_data, database_adapter: database_adapter)
        Order.new(order_data_instance)
      end
    end

    private

    def all_order_datas
      database_adapter.read(OrderData::DATA_SOURCE)
    end
  end
end
