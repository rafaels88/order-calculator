module OrderCalculator
  class Client
    def self.export_total_order(sources:, output:)
      calculator = OrderCalculator::Client.new(sources: sources)
      calculator.export_total_order(output)
    end

    attr_reader :sources, :database_adapter, :repository

    def initialize(sources:)
      @sources = sources
      @database_adapter = DatabaseAdapter.new(sources: sources)
      @repository = Repository.new(database_adapter: database_adapter)
    end

    def export_total_order(output_file_path)
      content = repository.all_orders.map { |o| normalize_result_content(o) }
      ResponseBuilder.new(content: content, template: output_file_path).build
    end

    private

    def normalize_result_content(order)
      [order.id, order.total_value]
    end
  end
end
