module OrderCalculator
  class ResponseBuilder
    attr_reader :content, :builder

    def initialize(content:, template:)
      @content = content
      @builder = CsvResponseBuilder.new(content: content, template: template)
    end

    def build
      builder.build
    end
  end
end
