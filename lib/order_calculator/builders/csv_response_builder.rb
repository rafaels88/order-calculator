module OrderCalculator
  class CsvResponseBuilder
    attr_reader :content, :template, :file_handler

    def initialize(content:, template:)
      @file_handler = CsvFileHandler.new
      @content = content
      @template = template
    end

    def build
      file_handler.write(template, content)
    end
  end
end
