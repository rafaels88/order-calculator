module OrderCalculator
  class Coupon
    extend Forwardable

    attr_reader :data_instance, :times_used
    def_delegators :@data_instance, :allowed_times, :expiry_date, :discount_type,
      :value

    def initialize(data_instance)
      @data_instance = data_instance
      @times_used = 0
    end

    def use!
      @times_used += 1
    end

    def is_allowed_to_use?
      expiry_date > Date.today && times_used < allowed_times
    end

    def is_percent_value?
      discount_type == "percent"
    end

    def is_absolute_value?
      discount_type == "absolute"
    end
  end
end
