module OrderCalculator
  class Order
    extend Forwardable
    attr_reader :data_instance
    def_delegators :@data_instance, :id, :coupon_id

    def initialize(data_instance)
      @data_instance = data_instance
    end

    def items
      data_instance.items.map { |di| OrderItem.new di }
    end

    def coupon
      @_coupon ||= Coupon.new(data_instance.coupon) unless data_instance.coupon.nil?
    end

    def real_total_value
      items.map(&:real_value).inject(0.0, :+)
    end

    def total_value
      value = real_total_value - discount_value
      value = value.to_i if value.to_f == value.to_i
      value
    end

    def discount_value
      [discount_value_for_items_quantity, discount_value_for_coupon].max
    end

    private

    def discount_value_for_items_quantity
      total_percentage_discount_by_items = (items.count * 5)
      total_percentage_discount_by_items = 40 if total_percentage_discount_by_items > 40
      real_total_value * (total_percentage_discount_by_items.to_f / 100)
    end

    def discount_value_for_coupon
      discount = 0
      items.each do |item|
        if coupon && coupon.is_allowed_to_use?
          discount += item.discount_value
          coupon.use!
        end
      end
      discount
    end
  end
end
