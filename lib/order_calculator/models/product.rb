module OrderCalculator
  class Product
    extend Forwardable

    attr_reader :data_instance
    def_delegators :@data_instance, :price, :id

    def initialize(data_instance)
      @data_instance = data_instance
    end
  end
end
