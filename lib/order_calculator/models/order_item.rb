module OrderCalculator
  class OrderItem
    extend Forwardable

    attr_reader :data_instance
    def_delegators :@data_instance,  :order_id

    def initialize(data_instance)
      @data_instance = data_instance
    end

    def product
      Product.new data_instance.product
    end

    def real_value
      product.price
    end

    def selling_value
      real_value - discount_value
    end

    def order
      Order.new data_instance.order
    end

    def discount_value
      if order.coupon && order.coupon.is_allowed_to_use?
        if order.coupon.is_percent_value?
          return product.price * (order.coupon.value / 100)
        elsif order.coupon.is_absolute_value?
          return order.coupon.value
        end
      end

      0.0
    end
  end
end
