require 'csv'
require 'spec_helper'
require_relative '../shared_contexts/data_sources'

describe OrderCalculator::CsvFileHandler do
  include_context "data sources"

  describe "#read" do

    context "when a file_path is given" do
      let(:csv_file_path) { csv_sources[:coupons] }

      it "returns file contents" do
        expected = CSV.open(csv_file_path, "r", :col_sep => ?,, :row_sep => ?\r).read
        result = subject.read(csv_file_path)

        expect(result).to be == expected
      end
    end
  end

  describe "#write" do
    context "when output_path and content are given" do
      let(:output_file_path) { "#{output_dir_path}/expected.csv" }
      let(:expected_content) { [['1', '2'], ['3', '4'], ['5', '6']] }

      it "creates csv file into given output_path" do
        subject.write(output_file_path, expected_content)

        generated_content = CSV.open(output_file_path, 'r', :col_sep => ?,, :row_sep => ?\r).read

        expect(generated_content).to be == expected_content
      end

      after :all do
        File.delete("#{File.dirname(__FILE__)}/../../tmp/expected.csv")
      end
    end
  end
end
