require 'csv'
require 'spec_helper'
require_relative '../shared_contexts/data_sources'

describe OrderCalculator::Client do
  describe ".export_total_order" do
    include_context "data sources"

    context "when all CSV files are given correctly" do

      before do
        described_class.export_total_order(sources: csv_sources, output: output_file_path)
      end

      it "does not raises error" do
        expect { described_class.export_total_order(sources: csv_sources, output: output_file_path) }.to_not raise_error
      end

      it "creates output file" do
        expect(File.exist?(output_file_path)).to be == true
      end

      it "writes output file with correct total values" do
        expected_content = CSV.open(expected_output_file_path, 'r', :col_sep => ?,, :row_sep => ?\r).read
        generated_content = CSV.open(output_file_path, 'r', :col_sep => ?,, :row_sep => ?\r).read

        expect(generated_content).to be == expected_content
      end

      after :all do
        File.delete("#{File.dirname(__FILE__)}/../../tmp/totals.csv")
      end
    end

    context "when any given source file is not in CSV format" do
      it "does raises InvalidSourceFileException error" do
        expect { described_class.export_total_order(sources: csv_sources_with_invalid, output: output_file_path) }.to raise_error OrderCalculator::InvalidSourceFileException
      end
    end

    context "when an unpermitted path to write is given for the output file" do
      let(:unpermitted_output_path) { "/etc/output.csv" }

      it "does raises OutputResultException error" do
        expect { described_class.export_total_order(sources: csv_sources, output: unpermitted_output_path) }.to raise_error OrderCalculator::OutputResultException
      end
    end
  end
end
