RSpec.shared_examples "data sources" do
  let(:output_dir_path) { "#{File.dirname(__FILE__)}/../../tmp" }
  let(:fixtures_dir_path) { "#{File.dirname(__FILE__)}/../../fixtures" }

  let(:output_file_path) { "#{output_dir_path}/totals.csv" }
  let(:coupons_file_path) { "#{fixtures_dir_path}/coupons.csv" }
  let(:products_file_path) { "#{fixtures_dir_path}/products.csv" }
  let(:orders_file_path) { "#{fixtures_dir_path}/orders.csv" }
  let(:order_items_file_path) { "#{fixtures_dir_path}/order_items.csv" }
  let(:expected_output_file_path) { "#{fixtures_dir_path}/expected_output.csv" }
  let(:invalid_csv_file_path) { "#{fixtures_dir_path}/invalid.csv" }

  let(:csv_sources) do
    {
      coupons: coupons_file_path,
      products: products_file_path,
      orders: orders_file_path,
      order_items: order_items_file_path,
    }
  end

  let(:csv_sources_with_invalid) do
    {
      coupons: coupons_file_path,
      products: products_file_path,
      orders: invalid_csv_file_path,
      order_items: order_items_file_path,
    }
  end
end
