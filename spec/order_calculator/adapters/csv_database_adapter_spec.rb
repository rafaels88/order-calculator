require 'csv'
require 'spec_helper'
require_relative '../shared_contexts/data_sources'

module OrderCalculator
  describe CsvDatabaseAdapter do
    include_context "data sources"

    describe "#read" do
      context "when a data source name is given" do
        subject { described_class.new(sources: csv_sources) }
        let(:data_source_name) { :products }

        it "returns CSV list os datas with formated hashes with associated data to this data source with no nil values" do
          expected_values = CSV.open(csv_sources[data_source_name], 'r', :col_sep => ?,, :row_sep => ?\r).read
          expected_content = expected_values.map do |data|
            { values: data } unless data.compact.empty?
          end
          expect(subject.read(data_source_name)).to be == expected_content.compact
        end
      end
    end
  end
end
