# Order

This is an exercise application for an Ecommerce Order Calculation, based on CSV files.

## Installation

You will need `bundle` gem installed.

    $ gem install bundle

And then execute:

    $ bin/setup

## Usage

    $ bin/run [CSV FILES]

Example:

    $ bin/run cupons.csv products.csv orders.csv order_items.csv output_totals.csv

For more informations:

    $ bin/run --help

## Development

To run tests:

    $ rake spec

To run application's console:

    $ bin/console

## License

This project is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
